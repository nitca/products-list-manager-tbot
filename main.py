import sys
from config.commands import run_command


def main():
    argv = sys.argv

    if len(argv) == 1:
        argv.append('help')

    command = argv[1]
    arguments = argv[2:]
    run_command(command, arguments)


if __name__ == '__main__':
    main()
