import os
import sqlite3

from datetime import datetime

from config import settings
from database.db import _get_database_connection_string

from alembic.config import Config as AlembicConfig
from alembic import command
from alembic.util.exc import CommandError

from abc import ABC
from abc import abstractmethod

import telebot


class BaseCommand(ABC):

    command = ''
    help_text = ''

    @abstractmethod
    def run_command(cls, *args, **kwargs):
        pass

    @classmethod
    def commands(cls):
        return cls.__subclasses__()

    @classmethod
    def _execute_command(cls, command_text, *args, **kwargs):

        commands = cls.commands()
        for command in commands:
            if command.command == command_text:
                command = command()
                command.run_command(*args, **kwargs)
                return command



class HelpCommand(BaseCommand):

    command = 'help'
    help_text = 'Help for usage'

    def run_command(self, *args, **kwargs):
        
        childs = BaseCommand.commands()

        print('\n\t\tHelp for usage telegram bot\n')
        for child in childs:
            # child = child()
            user_help = f"{child.command:15} -- {child.help_text:>14}"
            print(user_help)


class CreateDatabase(BaseCommand):

    command = 'create_database'
    help_text = 'Create sqlite3 database in root'

    def run_command(self, *args, **kwargs):
        engine = settings.DATABASE_CONNECTION.get('engine')
        assert engine == 'sqlite', 'Database\'s engine must be sqlite only'

        database = settings.DATABASE_CONNECTION.get('database')
        sqlite3.connect(database).close()

        print(f'Sqlite database success created in: {database}')


class MakeMigrations(BaseCommand):

    command = 'makemigrations'
    help_text = 'Create files migrations for database tables '\
            'usage: makemigrations migrations_name. Default: '\
            'migration_current_time'

    def default_message(self):

        current_time = datetime.now()
        current_time = current_time.strftime("%Y.%m.%d_%H:%M:%f")

        message = f"migration_at_{current_time}"
        return message


    def run_command(self, *args, **kwargs):

        if args == []:
            message = self.default_message()
        else:
            message = args[0]

        connection_string = _get_database_connection_string()

        alembic_cfg = AlembicConfig()
        alembic_cfg.set_main_option('script_location', 'migrations')
        alembic_cfg.set_main_option('sqlalchemy.url', connection_string)

        try:
            command.revision(alembic_cfg, autogenerate=True, message=message)
        except CommandError as exc:
            print(
                'Migrations already created. Run main.py migrate for apply'
            )


class Migrate(BaseCommand):

    command = 'migrate'
    help_text = 'Apply created migrations'

    def run_command(self, *args, **kwargs):
        
        connection_string = _get_database_connection_string()

        alembic_cfg = AlembicConfig()
        alembic_cfg.set_main_option('script_location', 'migrations')
        alembic_cfg.set_main_option('sqlalchemy.url', connection_string)

        try:
            command.upgrade(alembic_cfg, 'head')
        except CommandError as exc:
            print(
                f'Can\'t apply migrations: {exc}'
            )
        else:
            print('All migrations successfuly applyed')


class RunBot(BaseCommand):

    command = 'runbot'
    help_text = 'Run bot in polling if degut on or in webhook if debug is off'

    def run_command(self, *args, **kwargs):
        
        bot = telebot.TeleBot(settings.TOKEN)
        bot.delete_webhook()
        settings.BOT = bot
        
        from services import main_menu

        if settings.DEBUG:
            print('DEBUG is on\nBot polling running. Ctr-C for stop')
            bot.infinity_polling()

        else:
            from flask import Flask, request

            server = Flask(__name__)

            @server.route(f'/{settings.TOKEN}/', methods=['GET', 'POST'])
            def webhook():
                json_string = request.get_data().decode('utf-8')
                update = telebot.types.Update.de_json(json_string)
                bot.process_new_updates([update])
                return "Ok", 200
            
            bot.remove_webhook()
            url = f"{settings.WEBHOOK_DOMAIN}/{settings.TOKEN}/"
            bot.set_webhook(url=url)
            server.run(host='0.0.0.0', port=443, debug=True)


def run_command(command_text: str, *args, **kwargs) -> None:
    """
    Запуск команды

    Args:
        command: команда принятая при запуске

    ReturnsL
        None
    """

    BaseCommand._execute_command(command_text, *args, **kwargs)

