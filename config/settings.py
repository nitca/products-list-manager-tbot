from telebot import TeleBot

from pathlib import Path
import os


BASE_DIR = Path(__file__).resolve().parent.parent


BOT_DEBUG = os.getenv('BOT_DEBUG', '1')
DEBUG = BOT_DEBUG == '1'


BOT: TeleBot = None

TOKEN = os.getenv('TOKEN')

WEBHOOK_DOMAIN = os.getenv('WEBHOOK_DOMAIN')


LOG_DIR = BASE_DIR / 'logs'
LOG_FILENAME = 'dayli.log'


DATABASE_CONNECTION = {
    'engine': 'sqlite',
    'database': BASE_DIR / 'db.sqlite3',
}

# DATABASE_CONNECTION = {
#     'engine': 'postgresql',
#     'host': 'localhost',
#     'port': 5432,
#     'database': 'products_list',
#     'user': 'admin',
#     'password': 'tmp_pass',
# }
