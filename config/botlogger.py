import os
import sys

from logging import(
    Logger,
    StreamHandler,
    FileHandler,
    Formatter,
)

from config.settings import DEBUG
from config.settings import LOG_DIR
from config.settings import LOG_FILENAME



def _init_logger_handlers() -> list:
    """
    Создание хендлеров для логирования

    Args:
        None

    Returns:
        Список хендлеров
    """

    if DEBUG == True:
        level = 'DEBUG'
    else:
        level = 'INFO'

    formatter = Formatter(
        "%(asctime)s %(name)s %(levelname)s %(filename)s -- %(message)s"
    )
    stdout_handler = StreamHandler(
        sys.stdout
    )
    stdout_handler.setLevel(level)
    stdout_handler.setFormatter(formatter)

    if not os.path.exists(LOG_DIR):
        os.mkdir(LOG_DIR)

    logpath = f"{LOG_DIR}/{LOG_FILENAME}"

    file_handler = FileHandler(
        filename=logpath,
    )
    file_handler.setFormatter(formatter)

    handlers = [
        stdout_handler,
        file_handler,
    ]
    return handlers


def get_logger(name: str) -> Logger:
    """
    Получение объекта логирования

    Args:
        name: наименование логера

    Returns:
        Объект логера
    """

    handlers = _init_logger_handlers()

    logger = Logger(name)
    logger.handlers = handlers

    return logger
