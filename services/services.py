from telebot import TeleBot

from datetime import datetime
from sqlalchemy.orm import Session
from config.settings import BOT

from telebot_user_state import(
    UserStates,
    UserData,
)

from keyboard.menu_keyboards import(
    ListModelKeyboard,
    DepartmentsModelKeyboard,
    ProductsInlineKeyboard,
)

from database.models import(
    Users,
    Products,
    Departments,
    Lists,
    ProductsDepartments,
)

from config.botlogger import get_logger


user_states = UserStates()


logger = get_logger(__name__)


def go_to_menu(user_id: str, to_state: str, msg: str, keyboard: None) -> None:
    """
    Перевод пользователя в меню
    Отправляет сообщение пользователю с переданной клавиатурой

    Args:
        user_id: ID пользователя телеграм
        to_state: состояние в которое нужно переключить пользователя
        msg: сообщение отправляемое пользователю
        keyboard: объект клавиатуры отправляемый пользователю

    Returns:
        None
    """

    user_states.update_state(user_id, to_state)
    BOT.send_message(user_id, msg, reply_markup=keyboard)


def start_user(user_id: str, session: Session) -> None:
    """
    Логика запуска бота пользователем по команде /start

    Args:
        user_id: ID пользователя телеграм

    Returns:
        None
    """

    user = session.query(Users).filter(
        Users.user_id == user_id
    ).first()
    if user is None:
        current_datetime = datetime.now()
        user = Users(
            user_id=user_id,
            datetime=current_datetime,
        )
        session.add(user)
        session.commit()


def get_user_lists_keyboard(
        user_id: str, session: Session
    ) -> [ListModelKeyboard|None]:
    """
    Получение клавиатуры пользователя из списков для продуктов

    Args:
        user_id: ID пользователя телеграм
        session: объект сессии базы данных

    Returns:
        Объект клавиатуры или None в случае отсутствия кнопок
    """

    queryset = session.query(Lists).join(Users).filter(
        Users.user_id == user_id,
    ).all()
    if len(queryset) == 0:
        return None

    return ListModelKeyboard(queryset)


def get_departmets_keyboard(
        user_id: str,
        session: Session
    ) -> [DepartmentsModelKeyboard|None]:
    """
    Получение клавиатуры пользователя из списков отделов для продуктов

    Args:
        user_id: ID пользователя телеграм
        session: объект сессии базы данных

    Returns:
        Объект клавиатуры или None в случае отсутствия кнопок
    """

    queryset = session.query(Departments).join(Users).filter(
        Users.user_id == user_id
    ).all()
    if len(queryset) == 0:
        return None
    return DepartmentsModelKeyboard(queryset)


def add_list_name(user_id: str, text: str, session: Session) -> bool:
    """
    Добавление наименование списка для пользователя

    Args:
        user_id: ID пользователя телеграм
        text: наименование полученое от пользователя

    Returns:
        Статус добавления
    """

    try:
        user = session.query(Users).filter(
            Users.user_id == user_id
        ).first()
        product_list = session.query(Lists).filter(
            Lists.user_id == user.id,
            Lists.name == text
        ).first()
    except Exception as exc:
        logger.error(
            f'Не удалось добавить имя списка в базу данных: {exc}. '\
            f'Пользователь: {user_id} список: {text}'
        )
        return False

    if product_list is None:
        product_list = Lists(
            user_id=user.id,
            name=text
        )
        session.add(product_list)
        session.commit()
        return True
    return False


def delete_product_list(
    user_id: str, product_list_name: str, session: Session
    ) -> bool:
    """
    Удаление списка продуктов из базы данных

    Args:
        product_list_name: наименование списка продуктов
        session: объект сессии базы данных

    Returns:
        Статус удаления
    """

    try:
        product_list = session.query(Lists).join(Users).filter(
            Users.user_id == user_id,
            Lists.name == product_list_name
        ).first()
        if product_list:
            session.delete(product_list)
            session.commit()
            return True
    except Exception as exc:
        logger.error(
            f'Не удалось удалить список продуктов {product_list_name}: {exc}'
        )
    return False


def get_all_user_products(
    user_id: str, product_list: str, session: Session
    ) -> str:
    """
    Получение всех продуктов пользователя форматом:

        1) Яблоки 10 шт куплены Овощи и фрукты
    
    Args:
        user_id: ID пользователя телеграм
        product_list: выбранный список продуктов
        session: сессия подключённая к бд

    Returns:
        Сформированный список продуктов
        Или оповещение об пустом списке
    """

    products = session.query(Products).join(Lists).join(Users).filter(
        Users.user_id == user_id,
        Lists.name == product_list
    ).all()

    if len(products) == 0:
        return f'В списке {product_list} нет продуктов'

    final_string = f'Продукты в списке {product_list}:\n\n'
    for index, product in enumerate(products):
        index += 1
        byed = 'куплен' if product.byed else 'не куплен'
        department = product.departments[0].name
        tmp_str = f"{index}) {product.name} "\
                f"{product.total} {byed} {department}\n"
        final_string += tmp_str

    return final_string


def delete_saved_message(
        user_id: str, key: str, bot: TeleBot, user_data: UserData
    ) -> None:
    """
    Удаляет сохранённый ID сообщения в данных пользователя

    Args:
        user_id: ID пользователя телеграм
        key: ключ для поиска сообщения
        bot: объект телеграм бота
        user_data: объект данных пользователя

    Returns:
        None
    """

    message_id = user_data.get_data_by_key_and_id(user_id, key)
    logger.info(
        f'Удаление сообщения {message_id} пользователя {user_id}'
    )
    try:
        bot.delete_message(
            user_id,
            message_id,
        )
    except Exception as exc:
        logger.error(
            f'Не удалось удалить сообщение {message_id} пользователя '
            f'{user_id}: {exc}'
        )


def exists_department(
        user_id: str, department_name: str, session: Session,
        ) -> bool:
    """
    Проверка существования отдела продуктов

    Args:
        user_id: ID пользователя телеграм
        department_name: наименование отдела
        session: объект сессии базы данных

    Returns:
        Статус проверки
    """

    department = session.query(Departments).join(Users).filter(
        Users.user_id == user_id,
        Departments.name == department_name,
    ).first()

    if department is None:
        return False
    return True


def add_new_department(
        user_id: str, department_name: str, session: Session
    ) -> bool:
    """
    Добавление отдела продуктов в базу данных

    Args:
        user_id: ID пользователя телеграм
        department_name: наименование отдела
        session: объект сессии базы данных

    Returns:
        Статус добавления отдела продуктов
    """

    try:
        user = session.query(Users).filter(
            Users.user_id == user_id
        ).first()
        department = Departments(
            user=user.id,
            name=department_name,
        )
        session.add(department)
        session.commit()
        return True
    except Exception as exc:
        logger.error(
            f'Не удалось добавить отдел продуктов {department_name} '\
            f'пользователя {user_id}: {exc}'
        )
        return False


def delete_department(user_id: str, department: str, session: Session) -> bool:
    """
    Удаление отдела продуктов

    Args:
        user_id: ID пользователя телеграм
        department: наименование отдела продуктов
        session: объект сесси базы данных

    Returns:
        Статус удаления
    """

    try:
        del_department = session.query(Departments).filter(
            Departments.name == department
        ).first()
        if del_department:
            session.delete(del_department)
            session.commit()
    except Exception as exc:
        logger.error(
            f'Не удалось удалить отдел продуктов {department} '\
            f'пользователя {user_id}: {exc}'
        )
        return False
    return True


def update_department_name(
        user_id: str, department:str, new_name: str, session: Session
    ) -> bool:
    """
    Обновление наименования отдела продуктов

    Args:
        user_id: ID пользователя телеграм
        department: новое наименование отдела
        session: сессия подключения к базе данных

    Returns:
        Статус обновления
    """

    logger.info(
        f'Обновление наименования отдела продуктов {department} '\
        f'пользователя {user_id}'
    )
    try:
        department = session.query(Departments).join(Users).filter(
            Users.user_id == user_id,
            Departments.name == department
        ).first()
        department.name = new_name 
        session.add(department)
        session.commit()
    except Exception as exc:
        logger.error(
            f'Не удалось обновить наименование отдела продуктов {department} '\
                    f'пользователя {user_id}: {exc}'
        )
        return False
    return True


def product_exists(
    user_id: str, product_list_name: str, product_name: str, session: Session
    ) -> bool:
    """
    Проверка существования продукта в списке

    Args:
        user_id: ID пользователя телеграм
        product_list_name: наименование списа продуктов
        product_name: наименование продукта
        session: сессия подключения к базе данных

    Returns:
        Статус проверки
    """

    product_list = session.query(Lists).join(Users).filter(
        Users.user_id == user_id,
        Lists.name == product_list_name
    ).first()
    product = session.query(Products).filter(
        Products.lists == product_list.id,
        Products.name == product_name,
    ).first()
    if product is None:
        return False
    return True


def add_product(
        user_id: str,
        product_list_name: str,
        department: str,
        product_name: str,
        total: str,
        session: Session,
    ) -> bool:
    """
    Сохранение добавленного продукта

    Args:
        user_id: ID пользователя телеграм
        product_list_name: наименование списка
        department: отдел продуктов
        product_name: наименование продукта
        total: количество продукта
        session: сессия подключения к базе данных
    Returns:
        Статус добавления записи в базу
    """

    try:
        user = session.query(Users).filter(
            Users.user_id == user_id
        ).first()
        product_list = session.query(Lists).filter(
            Lists.user_id == user.id,
            Lists.name == product_list_name,
        ).first()
        department = session.query(Departments).filter(
            Departments.user == user.id,
            Departments.name == department
        ).first()

        product = Products(
            name=product_name,
            lists=product_list.id,
            total=total
        )
        session.add(product)
        product.departments.append(department)
        session.commit()
        return True
    except Exception as exc:
        logger.error(
            f'Не удалось добавить продукт пользователя {user_id} :'\
            f'{product_name=} {product_list_name=} {department=}: {exc}'
        )
        return False


def get_departments_product_kb_inl(
        user_id: str, department: str, session: Session
        ) -> [ProductsInlineKeyboard|None]:
    """
    Получение инлайн клавиатуры продуктов из отдела продуктов

    Args:
        user_id: ID пользователя телеграм
        department: наименование отдела продуктов
        session: сессия подключения к базе данных

    Returns:
        Клавиатура из инлайн кнопок продуктов или None в случае ошибки
    """

    try:
        queryset = session.query(Products).select_from(Users).join(
            Lists
        ).join(Products).join(ProductsDepartments).join(Departments).filter(
            Users.user_id == user_id,
            Departments.name == department,
        ).all()
        
        keyboard = ProductsInlineKeyboard(queryset)
    except Exception as exc:
        logger.error(
            f'Не удалось получить клавиатуру пользователя {user_id} '\
            f'отдела {department}: {exc}'
        )
        return None
    return keyboard


def update_product_byed(
        user_id: str, button_text: str, department: str, session: Session
    ) -> [ProductsInlineKeyboard|None]:
    """
    Обновление статуса покупки продукта

    Args:
        user_id: ID пользователя телеграм
        button_text: текст кнопки полученный при запросе
        session: сессия подключения к базе данных

    Returns:
        Обновлённая клавиатура или None в случае неудачи
    """

    button = button_text.split()
    if len(button) < 4:
        logger.error(
            f'При обновлении статуса покупки получена неверная кнопка '\
            f'пользователь: {user_id}, кнопка: {button_text}'
        )
        return None

    button = button[:-2]
    button = button[1:]

    text = ''
    for part in button:
        text += part + ' '
    text = text[:-1]

    logger.info(
        f'Пользователь {user_id} обновляет продукт {text}'
    )

    try:
        product = session.query(Products).select_from(Users).join(
            Lists
        ).join(Products).join(ProductsDepartments).join(Departments).filter(
            Users.user_id == user_id,
            Products.name == text,
        ).first()

        byed = not product.byed

        product.byed = byed
        session.add(product)
        session.commit()

        inl_keyboard = get_departments_product_kb_inl(
            user_id,
            department,
            session,
        )

    except Exception as exc:
        logger.error(
            f'Не удалось обновить статус продукта {text}: {exc}'
        )
        return None

    return inl_keyboard
