from telebot.types import(
    CallbackQuery,
    Message,
)

from datetime import datetime

from config.settings import BOT
from config.settings import DEBUG

from database.db import get_session

from database.models import(
    Users,
    Products,
    Departments,
    Lists,
    ProductsDepartments,
)

from telebot_user_state import(
    UserStates,
    UserData,
)

from messages_and_states import states
from messages_and_states import messages

from keyboard.menu_keyboards import(
    BackKeyboard,
    MainMenuKeyboard,
    DepartmentKeyboard,
    ListMenuKeyboard,
    ListEditMenuKeyboard,
    DeleteListInlineKeyboard,
    DepartmentsEditKeyboard,
    DeleteDepartmentInlineKeyboard,
    ProductsInlineKeyboard,
)

from services import services

from config.botlogger import get_logger


MESSAGE_TO_DELETE = 'message_to_delete'
LIST_NAME = 'list_name'
PRODUCT_NAME = 'product_name'
DEPARTMENT = 'department'

user_states = UserStates()
user_data = UserData(
    f"{MESSAGE_TO_DELETE} VARCHAR(32), "\
    f"{LIST_NAME} VARCHAR(256), "\
    f"{PRODUCT_NAME} VARCHAR(256), "\
    f"{DEPARTMENT} VARCHAR(256)"
)
session = get_session()


logger = get_logger(__name__)


@BOT.message_handler(commands=['start'])
def start(request: Message):
    """
    Команда /start
    Добавление пользователя в базу и перевод в главное меню
    """

    user_id = request.chat.id
    logger.info(f'Пользователь {user_id} выполнил команду /start')

    user_states.add_user(user_id)
    logger.debug(f'Пользователь {user_id} успешно добавлен в состояния')

    message_id = user_data.get_data_by_key_and_id(user_id, MESSAGE_TO_DELETE)
    logger.debug(f'Получен id сообщения на удаление: {message_id}')
    if message_id is not None and len(message_id) > 0:
        services.delete_saved_message(
            user_id, message_id, BOT, user_data,
        )

    user_data.drop_data(user_id)
    user_data.add_user_in_data(user_id)
    logger.debug(f'Пользователь {user_id} успешно добавлен в данные')

    services.start_user(user_id, session)
    services.go_to_menu(
        user_id,
        states.MAIN_MENU,
        messages.MAIN_MENU,
        keyboard=MainMenuKeyboard(),
    )


@BOT.message_handler(
    func=lambda request:
        user_states.is_current_state(request.chat.id, states.MAIN_MENU)
)
def main_menu(request: Message):
    """
    Главное меню
    Обработка кнопок
    """

    user_id = request.chat.id

    if request.text == MainMenuKeyboard.btn_add_product:
        
        product_list_kb = services.get_user_lists_keyboard(user_id, session)
        logger.debug(
            f'Переход пользователя {user_id} к добавлению продуктов.'\
            f'Полученные списки продуктов: {product_list_kb}'
        )

        if product_list_kb is None:
            BOT.send_message(
                user_id, messages.LISTS_NOT_FOUNDED,
            )
            services.go_to_menu(
                user_id,
                states.LISTS_NOT_FOUNDED,
                messages.ADD_LIST_NAME,
                BackKeyboard(),
            )
            logger.debug(
                f'У пользователя {user_id} списков не найдено. '\
                'Переход к добавлению списка'
            )
        else:
            services.go_to_menu(
                user_id,
                states.ADD_PRODUCT_LIST,
                messages.ADD_PRODUCT_LIST,
                product_list_kb,
            )
            logger.debug(
                f'У пользователя {user_id} найдены списки продуктов. '\
                'Переход к добавлению продуктов'
            )

    if request.text == MainMenuKeyboard.btn_products_lists:
        logger.debug(
            f'Пользователь {user_id} переход в меню управления списками'
        )
        product_list_kb = services.get_user_lists_keyboard(user_id, session)
        if product_list_kb == None:
            services.go_to_menu(
                user_id,
                states.ADD_LIST_NAME,
                messages.LISTS_NOT_FOUNDED,
                BackKeyboard(),
            )
            BOT.send_message(
                user_id,
                messages.ADD_LIST_NAME,
            )
            logger.debug(
                f'Переход {user_id} пользователя к добавлению нового списка'
            )
        else:
            services.go_to_menu(
                user_id,
                states.PRODUCTS_LIST,
                messages.PRODUCTS_LIST,
                product_list_kb,
            )
            logger.debug(
                f'Переход {user_id} пользователя к управлению списками'
            )

    if request.text == MainMenuKeyboard.btn_new_list:
        logger.debug(
            f'Переход пользователя {user_id} в создание списка продуктов'
        )
        services.go_to_menu(
            user_id,
            states.ADD_LIST_NAME,
            messages.ADD_LIST_NAME,
            BackKeyboard(),
        )

    if request.text == MainMenuKeyboard.btn_departments:
        logger.debug(
            f'Переход пользователя {user_id} в меню отделов продуктов'
        )
        services.go_to_menu(
            user_id,
            states.DEPARTMENTS_MENU,
            messages.DEPAERMENTS_MENU,
            DepartmentKeyboard(),
        )


@BOT.message_handler(
    func=lambda request:
        user_states.is_current_state(request.chat.id, states.ADD_PRODUCT_LIST)
)
def add_product_list(request: Message):
    """
    Начало добавления продукта
    Выбор списка продуктов
    """

    user_id = request.chat.id
    text = request.text

    if text == BackKeyboard.back:
        services.go_to_menu(
            user_id,
            states.MAIN_MENU,
            messages.MAIN_MENU,
            MainMenuKeyboard(),
        )
        return

    product_list = session.query(Lists).join(Users).filter(
        Users.user_id == user_id,
        Lists.name == text
    ).first()
    if product_list is None:
        message = messages.ADD_PRODUCT_LIST_NOT_EXISTS.format(
            text=text
        )
        BOT.send_message(
            user_id,
            message,
        )
    else:
        message = messages.ADD_PRODUCT_LIST_SUCCESS.format(
            text=text
        )
        user_data.add_data(user_id, LIST_NAME, text)
        departments_kb = services.get_departmets_keyboard(
            user_id,
            session,
        )
        if departments_kb is None:
            services.go_to_menu(
                user_id,
                states.DEPARTMENTS_NOT_FOUNDED,
                messages.DEPARTMENTS_NOT_FOUNDED,
                BackKeyboard(),
            )
        else:
            services.go_to_menu(
                user_id,
                states.ADD_PRODUCT_DEPARTMENT,
                messages.ADD_PRODUCT_DEPARTMENT,
                departments_kb,
            )


@BOT.message_handler(
    func=lambda request:
        user_states.is_current_state(
            request.chat.id, states.ADD_PRODUCT_DEPARTMENT
        )
)
def add_product_department(request: Message):
    """
    Добавление продукта
    Выбор отдела продуктов
    """

    user_id = request.chat.id
    text = request.text

    if text == BackKeyboard.back:
        services.go_to_menu(
            user_id,
            states.MAIN_MENU,
            messages.MAIN_MENU,
            MainMenuKeyboard(),
        )
        return

    if not services.exists_department(user_id, text, session):
        message = messages.DEPARTMENT_NOT_EXISTS.format(
            text=text
        )
        BOT.send_message(
            user_id,
            message
        )
    else:
        user_data.add_data(
            user_id,
            DEPARTMENT,
            text
        )
        services.go_to_menu(
            user_id,
            states.ADD_PRODUCT_NAME,
            messages.ADD_PRODUCT_NAME,
            BackKeyboard(),
        )


@BOT.message_handler(
    func=lambda request:
        user_states.is_current_state(request.chat.id, states.ADD_PRODUCT_NAME)
)
def add_product_name(request: Message):
    """
    Добавление продукта
    Ввод наименования
    """

    user_id = request.chat.id
    text = request.text

    if text == BackKeyboard.back:
        services.go_to_menu(
            user_id,
            states.MAIN_MENU,
            messages.MAIN_MENU,
            MainMenuKeyboard(),
        )
        return

    product_list_name = user_data.get_data_by_key_and_id(user_id, LIST_NAME)
    product_exists = services.product_exists(
        user_id,
        product_list_name,
        text,
        session
    )
    if product_exists:
        message = messages.PRODUCT_ALREADY_EXISTS.format(
            text=text
        )
        BOT.send_message(
            user_id,
            message,
        )
    else:
        user_data.add_data(user_id, PRODUCT_NAME, text)
        services.go_to_menu(
            user_id,
            states.ADD_PRODUCT_TOTAL,
            messages.ADD_PRODUCT_TOTAL,
            BackKeyboard()
        )


@BOT.message_handler(
    func=lambda request:
        user_states.is_current_state(request.chat.id, states.ADD_PRODUCT_TOTAL)
)
def add_product_total(request: Message):
    """
    Добавление продукта
    Ввод количества
    """

    user_id = request.chat.id
    text = request.text

    if text == BackKeyboard.back:
        services.go_to_menu(
            user_id,
            states.MAIN_MENU,
            messages.MAIN_MENU,
            MainMenuKeyboard(),
        )
        return

    text = text.replace(' ', '')

    product_list_name = user_data.get_data_by_key_and_id(
        user_id, LIST_NAME
    )
    department = user_data.get_data_by_key_and_id(
        user_id,
        DEPARTMENT
    )
    product_name = user_data.get_data_by_key_and_id(
        user_id,
        PRODUCT_NAME
    )
    product_added = services.add_product(
        user_id,
        product_list_name,
        department,
        product_name,
        text,
        session,
    )
    if product_added:
        message = messages.ADD_PRODUCT_SUCCESS.format(
            product_name=product_name,
            department=department,
            product_list_name=product_list_name
        )
        services.go_to_menu(
            user_id,
            states.MAIN_MENU,
            message,
            MainMenuKeyboard()
        )
    else:
        BOT.send_message(
            user_id,
            'Что-то пошло не так'
        )


@BOT.message_handler(
    func=lambda request:
        user_states.is_current_state(
            request.chat.id,
            states.ADD_LIST_NAME,
            states.LISTS_NOT_FOUNDED,
        )
)
def add_list_name_menu(request: Message):
    """
    Добавление имени списка
    """

    user_id = request.chat.id
    text = request.text

    if text == BackKeyboard.back:
        services.go_to_menu(
            user_id,
            states.MAIN_MENU,
            messages.MAIN_MENU,
            MainMenuKeyboard(),
        )
        return

    status = services.add_list_name(user_id, text, session)
    if status is True:
        message = messages.ADD_LIST_SECCESS.format(
            product_list=text
        )
        BOT.send_message(
            user_id,
            message,
        )
        if user_states.is_current_state(user_id, states.LISTS_NOT_FOUNDED):
            product_list_kb = services.get_user_lists_keyboard(user_id, session)
            services.go_to_menu(
                user_id,
                states.ADD_PRODUCT_LIST,
                messages.ADD_PRODUCT_LIST,
                product_list_kb,
            )
        else:
            services.go_to_menu(
                user_id,
                states.MAIN_MENU,
                messages.MAIN_MENU,
                MainMenuKeyboard(),
            )
    else:
        message = messages.ADD_LIST_NAME_BUSY.format(
            product_list=text
        )
        BOT.send_message(
            user_id,
            message,
        )


@BOT.message_handler(
    func=lambda request:
        user_states.is_current_state(request.chat.id, states.PRODUCTS_LIST)
)
def product_list(request: Message):
    """
    Список продуктов
    """

    user_id = request.chat.id
    text = request.text

    if text == BackKeyboard.back:
        services.go_to_menu(
            user_id,
            states.MAIN_MENU,
            messages.MAIN_MENU,
            MainMenuKeyboard(),
        )
        return

    product_list = session.query(Lists).join(Users).filter(
        Lists.name == text,
        Users.user_id == user_id,
    ).first()
    if product_list is None:
        message = messages.PRODUCT_LIST_NOT_EXISTS.format(
            text=text
        )
        BOT.send_message(
            user_id,
            message
        )
    else:
        message = messages.PRODUCT_LIST_MENU.format(
            text=text
        )
        user_data.add_data(
            user_id, LIST_NAME, text
        )
        services.go_to_menu(
            user_id,
            states.PRODUCT_LIST_MENU,
            message,
            ListMenuKeyboard(),
        )


@BOT.message_handler(
    func=lambda request:
        user_states.is_current_state(request.chat.id, states.PRODUCT_LIST_MENU)
)
def product_list_menu(request: Message):
    """
    Меню управления списком
    """

    user_id = request.chat.id
    text = request.text

    if text == BackKeyboard.back:
        queryset = session.query(Lists).join(Users).filter(
            Users.user_id == user_id
        ).all()
        product_list_kb = services.get_user_lists_keyboard(user_id, session)
        services.go_to_menu(
            user_id,
            states.PRODUCTS_LIST,
            messages.PRODUCTS_LIST,
            product_list_kb,
        )
        return

    product_list_name = user_data.get_data_by_key_and_id(user_id, LIST_NAME)
    if text == ListMenuKeyboard.btn_look_all_products:
        message = services.get_all_user_products(
            user_id,
            product_list_name,
            session,
        )
        BOT.send_message(
            user_id,
            message
        )

    if text == ListMenuKeyboard.btn_look_current:
        departments_kb = services.get_departmets_keyboard(
            user_id,
            session,
        )
        services.go_to_menu(
            user_id,
            states.PRODUCT_LIST_DEPARTMENTS,
            messages.PRODUCT_LIST_DEPARTMENTS,
            departments_kb,
        )

    if text == ListMenuKeyboard.btn_edit_list:
        message = messages.PRODUCT_LIST_EDIT_MENU.format(
            text=product_list_name
        )
        services.go_to_menu(
            user_id,
            states.PRODUCT_LIST_EDIT_MENU,
            message,
            ListEditMenuKeyboard(),
        )


@BOT.message_handler(
    func=lambda request:
        user_states.is_current_state(
            request.chat.id, states.PRODUCT_LIST_EDIT_MENU
        )
)
def product_edit_list_menu(request: Message):
    """
    Меню редактирование списка продуктов
    """

    user_id = request.chat.id
    text = request.text
    product_list_name = user_data.get_data_by_key_and_id(user_id, LIST_NAME)

    if text == BackKeyboard.back:
        message = messages.PRODUCT_LIST_MENU.format(
            text=product_list_name
        )
        services.go_to_menu(
            user_id,
            states.PRODUCT_LIST_MENU,
            message,
            ListMenuKeyboard(),
        )
        return

    if text == ListEditMenuKeyboard.btn_delete:
        message = messages.PRODUCT_LIST_EDIT_DELETE.format(
            text=product_list_name
        )
        BOT.send_message(
            user_id,
            text=messages.MAKE_CHOICE,
            reply_markup=BackKeyboard()
        )
        services.go_to_menu(
            user_id,
            states.PRODUCT_LIST_EDIT_DELETE,
            message,
            DeleteListInlineKeyboard(),
        )
        message_id = request.message_id + 2
        user_data.add_data(user_id, MESSAGE_TO_DELETE, message_id)


@BOT.callback_query_handler(
    func=lambda request: DeleteListInlineKeyboard.inline_keyboard_user(
        request.data
    )
)
def delete_yes_no_inline(request: CallbackQuery):
    """
    Обработка клавиатуры удаления списка
    """

    user_id = request.from_user.id
    product_list_name = user_data.get_data_by_key_and_id(user_id, LIST_NAME)

    if request.data == DeleteListInlineKeyboard.btn_yes.get('callback'):
        services.delete_product_list(
            user_id,
            product_list_name,
            session,
        )
        services.go_to_menu(
            user_id,
            states.MAIN_MENU,
            messages.MAIN_MENU,
            MainMenuKeyboard(),
        )
    elif request.data == DeleteListInlineKeyboard.btn_no.get('callback'):
        message = messages.PRODUCT_LIST_EDIT_MENU.format(
            text=product_list_name
        )
        services.go_to_menu(
            user_id,
            states.PRODUCT_LIST_EDIT_MENU,
            message,
            ListEditMenuKeyboard(),
        )
    services.delete_saved_message(
        user_id,
        MESSAGE_TO_DELETE,
        BOT,
        user_data
    )


@BOT.message_handler(
    func=lambda request:
        user_states.is_current_state(
            request.chat.id, states.PRODUCT_LIST_EDIT_DELETE
        )
)
def product_list_edit_delete(request: Message):
    """
    Подменю удаления списка продуктов
    """

    user_id = request.chat.id
    text = request.text
    product_list_name = user_data.get_data_by_key_and_id(user_id, LIST_NAME)
    message_to_delete = user_data.get_data_by_key_and_id(
        user_id, MESSAGE_TO_DELETE
    )

    if text == BackKeyboard.back:
        try:
            BOT.delete_message(user_id, message_to_delete)
        except Exception:
            pass

        message = messages.PRODUCT_LIST_EDIT_MENU.format(
            text=product_list_name
        )
        services.go_to_menu(
            user_id,
            states.PRODUCT_LIST_EDIT_MENU,
            message,
            ListEditMenuKeyboard(),
        )
        return


@BOT.message_handler(
    func=lambda request:
        user_states.is_current_state(
            request.chat.id, states.PRODUCT_LIST_DEPARTMENTS
        )
)
def product_list_departments(request: Message):
    """
    Отделы продуктов из списка продуктов
    """

    user_id = request.chat.id
    text = request.text
    product_list_name = user_data.get_data_by_key_and_id(user_id, LIST_NAME)

    if text == BackKeyboard.back:
        message = messages.PRODUCT_LIST_MENU.format(
            text=product_list_name
        )
        services.go_to_menu(
            user_id,
            states.PRODUCT_LIST_MENU,
            message,
            ListMenuKeyboard(),
        )
        return

    department_exists = services.exists_department(
        user_id,
        text,
        session
    )
    if not department_exists:
        logger.warning(
            f'Попытка пользователем {user_id} выбрать несуществующий '\
            'отдел продуктов'
        )
        message = messages.DEPARTMENT_NOT_EXISTS.format(
            text=text
        )
        BOT.send_message(
            user_id,
            message
        )
    else:
        logger.debug(
            f'Переход пользователя {user_id} к просмотру своих отделов'
        )

        user_data.add_data(
            user_id,
            DEPARTMENT,
            text,
        )
        inl_keyboard = services.get_departments_product_kb_inl(
            user_id,
            text,
            session,
        )

        if inl_keyboard is None:
            message = messages.DEPARTMENTS_NO_PRODUCTS.format(
                text=text
            )
            BOT.send_message(
                user_id,
                message
            )
        else:
            BOT.send_message(
                user_id,
                messages.DEPARTMENTS_LIST,
                reply_markup=inl_keyboard,
            )


@BOT.callback_query_handler(
    func=lambda request: ProductsInlineKeyboard.inline_keyboard_user(
        request.data
    )
)
def departmen_product_inline(request: CallbackQuery):
    """
    Обраотка клавиатуры обновления статуса продукта
    """

    user_id = request.message.chat.id
    button_text = ProductsInlineKeyboard.get_text_button(request)
    department = user_data.get_data_by_key_and_id(user_id ,DEPARTMENT)

    keyboard = services.update_product_byed(
        user_id,
        button_text,
        department,
        session,
    )
    if keyboard is None:
        services.go_to_menu(
            user_id,
            states.MAIN_MENU,
            messages.ERROR,
            MainMenuKeyboard(),
        )
        return

    message_id = request.message.message_id
    BOT.edit_message_reply_markup(
        user_id,
        message_id,
        reply_markup=keyboard
    )


@BOT.message_handler(
    func=lambda request:
        user_states.is_current_state(request.chat.id, states.DEPARTMENTS_MENU)
)
def departments_menu(request: Message):
    """
    Меню создания и управление отделов
    """

    user_id = request.chat.id
    text = request.text

    if text == DepartmentKeyboard.back:
        services.go_to_menu(
            user_id,
            states.MAIN_MENU,
            messages.MAIN_MENU,
            MainMenuKeyboard(),
        )
        logger.debug(
            f'Возврат пользователя {user_id} из меню управления '\
            'отделами продуктов в главное меню'
        )
        return

    if text == DepartmentKeyboard.btn_add_department:
        logger.debug(
            f'Меню управления отделами продуктов пользователь {user_id} '\
            'создаёт новый отдел'
        )
        services.go_to_menu(
            user_id,
            states.DEPARTMENTS_ADD_NAME,
            messages.DEPARTMENT_CREATE_NAME,
            BackKeyboard()
        )

    if text == DepartmentKeyboard.btn_edit_department:
        logger.debug(
            f'Пользователь {user_id} переходит к редактированию своих отделов'
        )
        departments_kb = services.get_departmets_keyboard(
            user_id,
            session,
        )
        if departments_kb is None:
            BOT.send_message(
                user_id,
                messages.DEPARTMENTS_USER_NO_DEPARTMENTS
            )
        else:
            services.go_to_menu(
                user_id,
                states.DEPARTMENTS_EDIT,
                messages.DEPARTMENTS_EDIT,
                departments_kb,
            )


@BOT.message_handler(
    func=lambda request:
        user_states.is_current_state(
            request.chat.id,
            states.DEPARTMENTS_ADD_NAME,
            states.DEPARTMENTS_NOT_FOUNDED,
        )
)
def add_departments_name(request: Message):
    """
    Добавление наименования отдела продуктов
    """

    user_id = request.chat.id
    text = request.text

    if text == DepartmentKeyboard.back:
        services.go_to_menu(
            user_id,
            states.MAIN_MENU,
            messages.MAIN_MENU,
            MainMenuKeyboard(),
        )
        return

    if services.exists_department(user_id, text, session):
        message = messages.DEPARTMENT_NAME_NOT_VALID.format(
            text=text
        )
        BOT.send_message(
            user_id,
            message,
        )
    else:
        status = services.add_new_department(
            user_id,
            text,
            session
        )
        if status == False:
            services.go_to_menu(
                user_id,
                states.MAIN_MENU,
                messages.ERROR,
                MainMenuKeyboard(),
            )
        else:
            message = messages.DEPARTMENT_ADD_NAME_SUCCESS.format(
                text=text
            )
            BOT.send_message(
                user_id,
                message,
            )
            if user_states.is_current_state(
                user_id, states.DEPARTMENTS_NOT_FOUNDED
            ):
                services.go_to_menu(
                    user_id,
                    states.ADD_PRODUCT_DEPARTMENT,
                    messages.ADD_PRODUCT_DEPARTMENT,
                    services.get_departmets_keyboard(user_id, session),
                )
            else:
                services.go_to_menu(
                    user_id,
                    states.DEPARTMENTS_MENU,
                    messages.DEPAERMENTS_MENU,
                    DepartmentKeyboard(),
                )


@BOT.message_handler(
    func=lambda request:
        user_states.is_current_state(request.chat.id, states.DEPARTMENTS_EDIT)
)
def departments_edit(request: Message):
    """
    Выбор отдела для редактирования
    """

    user_id = request.chat.id
    text = request.text

    if text == DepartmentKeyboard.back:
        services.go_to_menu(
            user_id,
            states.DEPARTMENTS_MENU,
            messages.DEPAERMENTS_MENU,
            DepartmentKeyboard(),
        )
        return

    exists_department = services.exists_department(
        user_id,
        text,
        session,
    )
    if not exists_department:
        logger.warning(
            f'Попытка выбора пользователем {user_id} несуществующего '\
            f'отдела продуктов {text}'
        )
        message = messages.DEPARTMENT_NOT_EXISTS.format(
            text=text,
        )
        BOT.send_message(
            user_id,
            message
        )
    else:
        logger.debug(
            f'Пользователь {user_id} выбрал отдел продуктов {text}'
        )
        user_data.add_data(
            user_id, DEPARTMENT, text,
        )
        message = messages.DEPARTMENTS_EDIT_MENU.format(
            text=text,
        )
        services.go_to_menu(
            user_id,
            states.DEPARTMENTS_EDIT_MENU,
            message,
            DepartmentsEditKeyboard(),
        )


@BOT.message_handler(
    func=lambda request:
        user_states.is_current_state(
            request.chat.id, states.DEPARTMENTS_EDIT_MENU
        )
)
def departments_edit_menu(request: Message):
    """
    Выбор редактирования отдела
    """

    user_id = request.chat.id
    text = request.text

    if text == DepartmentsEditKeyboard.back:
        departments_kb = services.get_departmets_keyboard(
            user_id,
            session,
        )
        services.go_to_menu(
            user_id,
            states.DEPARTMENTS_EDIT,
            messages.DEPARTMENTS_EDIT,
            departments_kb,
        )
        return

    department = user_data.get_data_by_key_and_id(
        user_id, DEPARTMENT,
    )

    if text == DepartmentsEditKeyboard.btn_edit_name:
        message = messages.DEPARTMENT_EDIT_NAME.format(
            text=department
        )
        services.go_to_menu(
            user_id,
            states.DEPARTMENTS_EDIT_NAME,
            message,
            BackKeyboard(),
        )

    if text == DepartmentsEditKeyboard.btn_delete:
        message_to_delete = request.message_id + 2
        user_data.add_data(
            user_id, MESSAGE_TO_DELETE, message_to_delete
        )
        message = messages.DEPARTMENTS_DELETE_QUESTION.format(
            text=department,
        )
        BOT.send_message(
            user_id,
            messages.DEPARTMENTS_DELETE,
            reply_markup=BackKeyboard()
        )
        services.go_to_menu(
            user_id,
            states.DEPARTMENTS_DELETE,
            message,
            DeleteDepartmentInlineKeyboard(),
        )


@BOT.callback_query_handler(
    lambda request: DeleteDepartmentInlineKeyboard.inline_keyboard_user(
        request.data
    )
)
def departments_delete_inline(request: CallbackQuery):
    """
    Инлайн клавиатура удаления отдела продуктов
    """

    user_id = request.from_user.id
    department = user_data.get_data_by_key_and_id(user_id, DEPARTMENT)

    if request.data == DeleteDepartmentInlineKeyboard.btn_yes.get('callback'):
        department_deleted = services.delete_department(
            user_id,
            department,
            session,
        )
        if department_deleted:
            message = messages.DEPARTMENT_SUCCESS_DELETED.format(
                text=department
            )
        else:
            message = messages.DEPARTMENT_FAILED_DELETED.format(
                text=department
            )

        BOT.send_message(
            user_id,
            message,
        )

        services.go_to_menu(
            user_id,
            states.DEPARTMENTS_MENU,
            messages.DEPAERMENTS_MENU,
            DepartmentKeyboard(),
        )
    elif request.data == DeleteDepartmentInlineKeyboard.btn_no.get('callback'):
        message = messages.DEPARTMENTS_EDIT_MENU.format(
            text=department
        )
        services.go_to_menu(
            user_id,
            states.DEPARTMENTS_EDIT_MENU,
            message,
            DepartmentsEditKeyboard(),
        )
    services.delete_saved_message(
        user_id,
        MESSAGE_TO_DELETE,
        BOT,
        user_data
    )


@BOT.message_handler(
    func=lambda request:
        user_states.is_current_state(
            request.chat.id, states.DEPARTMENTS_EDIT_NAME
        )
)
def departments_edit_name(request: Message):
    """
    Редактирование наименования отдела
    """

    user_id = request.chat.id
    text = request.text
    department = user_data.get_data_by_key_and_id(
        user_id, DEPARTMENT,
    )

    if text == DepartmentsEditKeyboard.back:
        message = messages.DEPARTMENTS_EDIT_MENU.format(
            text=department
        )
        services.go_to_menu(
            user_id,
            states.DEPARTMENTS_EDIT_MENU,
            message,
            DepartmentsEditKeyboard(),
        )
        return

    department_exists = services.exists_department(
        user_id,
        text,
        session
    )
    if department_exists:
        message = messages.DEPARTMENT_ALREADY_EXISTS.format(
            exists=text,
            text=department,
        )
        BOT.send_message(
            user_id,
            message,
        )
    else:
        department_updated = services.update_department_name(
            user_id,
            department,
            text,
            session,
        )
        if department_updated:
            message = messages.DEPARTMENT_NAME_SUCCESS_UPDATED.format(
                department=department,
                text=text,
            )
        else:
            message = messages.DEPARTMENT_NAME_FAILED_UPDATED.format(
                department=department,
                text=text,
            )

        BOT.send_message(
            user_id,
            message,
        )
        services.go_to_menu(
            user_id,
            states.MAIN_MENU,
            messages.MAIN_MENU,
            MainMenuKeyboard(),
        )
