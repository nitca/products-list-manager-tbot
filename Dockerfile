FROM python:3.10

COPY . .

RUN python -m pip install -r requirements.txt
RUN python main.py create_database
RUN python main.py migrate


CMD ["python", "main.py", "runbot"]
