from sqlalchemy import(
    Table,
    Column,
    Boolean,
    String,
    DateTime,
    Integer,
    ForeignKey,
)

from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base()


ProductsDepartments = Table(
    'products_departments',
    Base.metadata,
    Column(
        'id',
        Integer,
        primary_key=True
    ),
    Column(
        'products_id',
        Integer,
        ForeignKey('products.id'),
    ),
    Column(
        'departments_id',
        Integer,
        ForeignKey('departments.id'),
    )
)

class Users(Base):

    __tablename__ = 'users'

    id = Column(
        Integer, primary_key=True,
    )
    user_id = Column(
        String(64),
    )
    blocked = Column(
        Boolean, default=False,
    )
    datetime = Column(
        DateTime,
    )

    lists = relationship('Lists', backref='users')


class Lists(Base):

    __tablename__ = 'lists'

    id = Column(
        Integer, primary_key=True,
    )
    user_id = Column(
        Integer, ForeignKey('users.id'), nullable=True,
    )
    name = Column(
        String(128),
    )

    products = relationship(
        'Products', backref='product_lists',
    )


class Products(Base):

    __tablename__ = 'products'

    lists = Column(
        Integer, ForeignKey('lists.id'), nullable=True,
    )
    id = Column(
        Integer, primary_key=True,
    )
    byed = Column(
        Boolean, default=False,
    )
    name = Column(
        String(128),
    )
    total = Column(
        String(32), default='1 шт',
    )

    departments = relationship(
        'Departments',
        secondary=ProductsDepartments,
        back_populates='products',
    )


class Departments(Base):

    __tablename__ = 'departments'

    id = Column(
        Integer, primary_key=True,
    )
    user = Column(
        Integer, ForeignKey('users.id'),
    )
    name = Column(
        String(128),
    )

    products = relationship(
        'Products',
        secondary=ProductsDepartments,
        back_populates='departments'
    )
