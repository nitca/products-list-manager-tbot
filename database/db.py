from sqlalchemy import Engine
from sqlalchemy.orm import Session

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from datetime import datetime

from config.settings import DATABASE_CONNECTION

from database.models import(
    Users,
    Lists,
    Products,
    Departments,
)

from config.botlogger import get_logger


logger = get_logger(__name__)


def _get_database_connection_string() -> str:
    """
    Получение строки для подключения к базе данных

    Args:
        None

    Returns:
        Строка для подключения к базе данных
    """

    logger.debug(
        f'Инициализация подключения к базе данных '\
        f'с настройками: {DATABASE_CONNECTION}'
    )

    engine = DATABASE_CONNECTION.get('engine')
    host = DATABASE_CONNECTION.get('host')
    port = DATABASE_CONNECTION.get('port')
    database = DATABASE_CONNECTION.get('database')
    user = DATABASE_CONNECTION.get('user')
    password = DATABASE_CONNECTION.get('password')

    if user is None and password is None:
        logger.debug('Логин и пароль базы данных не заданы в настройках')
        login_string = ''
    else:
        login_string = f'{user}:{password}'
        logger.debug(
            f'Получена стройка логина и пароля базы данных: {login_string}'
        )

    if host is None and port is None:
        logger.debug('Хост и порт базы данных не заданы в настройках')
        host_string = ''
    else:
        host_string = f'@{host}{port}'
        logger.debug(
            f'Получена стройка хоста и порта базы данных: {host_string}'
        )

    connection_string = f'{engine}:/{login_string}/{host_string}/{database}'
    logger.debug(
        f'Сформирована строка подключения к базе данных: {connection_string}'
    )

    return connection_string


def _init_engine_database() -> Engine:
    """
    Подключение к базе данных с данными из настроек

    Args:
        None

    Returns:
        Объект подключения к базе данных
    """

    connection_string = _get_database_connection_string()
    logger.debug('Создание подключения к базе данных')
    try:
        engine = create_engine(connection_string)
    except Exception as exc:
        logger.error(
            f'Не удалось подключиться к базе данных: {exc}. Проверьте настройки'
        )
        exit(1)

    logger.debug('Подключение к базе данных успешно выполнено')
    return engine


def get_session() -> Session:
    """
    Получение объекта сессии ОРМ

    Args:
        None

    Returns:
        Объект сессии, подключённый к базе данных
    """

    logger.debug(
        'Начало создания объекта сессии'
    )
    logger.debug(
        'Инициализация подключения к базе данных'
    )

    engine = _init_engine_database()
    logger.debug(
        f'Объект подключения к базе данных успешно получен: {engine}'
    )

    logger.debug(
        'Инициализация объекта сессии'
    )
    try:
        Session = sessionmaker(bind=engine)
        session = Session()
    except Exception as exc:
        logger.error(
            f'Не удалось получить объект сессии: {exc}'
        )
        exit(2)

    logger.debug('Сессия успешно создана')
    return session
