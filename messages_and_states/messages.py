WELCOME_MESSAGE = 'Добро пожаловать!'
MAKE_CHOICE = 'Выберите действие:'

MAIN_MENU = 'Главное меню'
DEPAERMENTS_MENU = 'Управление отделами продуктов'

LISTS_NOT_FOUNDED = 'У вас не найдено списков продуктов. '\
        'Пожалуйста, добавьте список для удобной сортировки'
DEPARTMENTS_NOT_FOUNDED = 'У вас не найдено отдело продуктов. '\
        'Пожалуйста, добавьте отдел для удобной сортировки'

ADD_LIST_NAME = 'Введите наименование списка продуктов:'
ADD_LIST_SECCESS = 'Список "{product_list}" успешно добавлен!'
ADD_LIST_NAME_BUSY = 'Список "{product_list}" уже занят. Придумайте другое имя:'

ADD_PRODUCT_LIST = 'Выберите список продуктов:'
ADD_PRODUCT_LIST_NOT_EXISTS = 'Список "{text}" не найден.\n' + ADD_PRODUCT_LIST
ADD_PRODUCT_LIST_SUCCESS = 'Выбран список продуктов "{text}"'

ADD_PRODUCT_DEPARTMENT = 'Выберите отдел продуктов:'
ADD_PRODUCT_NAME = 'Введите название продукта:'
ADD_PRODUCT_TOTAL = 'Введите количество продуктов (1шт 1кг):'

PRODUCT_ALREADY_EXISTS = 'Продукт "{text}" уже добавлен в список ' +\
        ADD_PRODUCT_NAME

PRODUCTS_LIST = 'Выберите список продуктов:'
PRODUCT_LIST_NOT_EXISTS = 'Список продуктов "{text}" не найден'
PRODUCT_LIST_MENU = 'Управление списком продуктов "{text}":'
PRODUCT_LIST_EDIT_MENU = 'Редактирование списка "{text}":'
PRODUCT_LIST_DEPARTMENTS = 'Выберите отдел продуктов:'
PRODUCT_LIST_DEPARTMENTS_PRODUCTS = 'Продукты из отдела продуктов "{text}"'
PRODUCT_LIST_EDIT_DELETE = 'Удалить список "{text}" ?'

DEPARTMENT_CREATE_NAME = 'Введите наименование отдела продуктов:'
DEPARTMENT_NAME_NOT_VALID = 'Наименование отдела продуктов "{text}" занято '\
        'или не прошло проверку.\nВведите наименование отдела продуктов:'
DEPARTMENT_ADD_NAME_SUCCESS = 'Наименование отдела продуктов "{text}" '\
        'успешно добавлено'
DEPARTMENT_NOT_EXISTS = 'Наименование отдела продуктов "{text}" не найдено\n' +\
    ADD_PRODUCT_DEPARTMENT
DEPARTMENTS_LIST = 'Выберите список отделов:'
DEPARTMENTS_EDIT = 'Выберите список отделов для редактирования:'
DEPARTMENTS_EDIT_MENU = 'Редактирование отдела "{text}":'
DEPARTMENTS_NO_PRODUCTS = 'В отделе "{text}" не добавлено продуктов\n' +\
    DEPARTMENTS_LIST
DEPARTMENTS_USER_NO_DEPARTMENTS = 'У вас пока нет отделов продуктов '\
        'добавьте новый отдел для удобный сортировки продуктов'
DEPARTMENTS_DELETE_QUESTION = 'Удалить "{text}" отдел продуктов?'
DEPARTMENTS_DELETE = 'Удаление отдела продуктов'

DEPARTMENT_SUCCESS_DELETED = 'Отдел "{text}" успешно удалён'
DEPARTMENT_FAILED_DELETED = 'Не удалось удалить отдел "{text}". '\
        'Пожалуйста, попробуйте позже'

DEPARTMENT_EDIT_NAME = 'Введите другое название отдела "{text}":'
DEPARTMENT_ALREADY_EXISTS = 'Отдел продуктов "{exists}" уже существует\n'+\
        DEPARTMENT_EDIT_NAME
DEPARTMENT_NAME_SUCCESS_UPDATED = 'Наименование отдела "{department}" '\
        'успешно заменено на "{text}"'
DEPARTMENT_NAME_FAILED_UPDATED = 'Не удалось переименовать отдела '\
        '"{department}" в "{text}". Пожалуйста, попробуйте позже'

ERROR = 'Не удалось завершить операцию. Пожалуйста, попробуйте позже'

NOT_NUMBER = 'Введено не число'

ADD_PRODUCT_SUCCESS = 'Добавлен продукт "{product_name}" в отдел "{department}" '\
        'состоящий в списке "{product_list_name}"'
