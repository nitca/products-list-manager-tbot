from keyboard.keyboard import(
    TelegramKeyboardReply,
    TelegramModelKeyboardReply,
    TelegramKeyboardInline,
    TelegramModelKeyboardInline,
)

from database.models import(
    Lists,
    Departments,
    Products,
)

class BackKeyboard(TelegramKeyboardReply):
    pass


class MainMenuKeyboard(TelegramKeyboardReply):
    btn_add_product = 'Добавить продукт [🍎]'
    btn_products_lists = 'Списки продуктов [🛒]'
    btn_new_list = 'Создать новый список [🗒]'
    btn_departments = 'Отделы [🚪]'

    class Meta:
        main_menu = True


class DepartmentKeyboard(TelegramKeyboardReply):
    btn_add_department = 'Добавить отдел [➕]'
    btn_edit_department = 'Редактировать отдел [📝]'


class ListModelKeyboard(TelegramModelKeyboardReply):

    class Meta:
        model = Lists
        field = 'name'


class ListMenuKeyboard(TelegramKeyboardReply):
    btn_edit_list = 'Редактировать список [✏️]'
    btn_look_current = 'Отделы [💠]'
    btn_look_all_products = 'Просмотреть все продукты [👀]'


class ListEditMenuKeyboard(TelegramKeyboardReply):
    btn_products = 'Продукты [🥒]'
    btn_name = 'Наименование списка [📝]'
    btn_delete = 'Удалить список [❌]'


class DeleteListInlineKeyboard(TelegramKeyboardInline):
    btn_yes = {
        'text': 'Да',
        'callback': 'delete_product_list_yes'
    }
    btn_no = {
        'text': 'Нет',
        'callback': 'delete_product_list_no'
    }


class DepartmentsModelKeyboard(TelegramModelKeyboardReply):

    class Meta:
        model = Departments
        field = 'name'


class DepartmentsEditKeyboard(TelegramKeyboardReply):

    btn_edit_name = 'Поменять название [✍️]'
    btn_delete = 'Удалить отдел продуктов [❌]'


class ProductsInlineKeyboard(TelegramModelKeyboardInline):

    class Meta:
        model = Products
        fields = [
            'name',
            'total',
            'byed',
        ]


class DeleteDepartmentInlineKeyboard(TelegramKeyboardInline):
    btn_yes = {
        'text': 'Да',
        'callback': 'delete_department_yes'
    }
    btn_no = {
        'text': 'Нет',
        'callback': 'delete_department_no'
    }
