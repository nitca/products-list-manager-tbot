from abc import(
    ABC,
    abstractmethod,
)

from telebot.types import(
    ReplyKeyboardMarkup,
    KeyboardButton,
    InlineKeyboardMarkup,
    InlineKeyboardButton,
    CallbackQuery,
)


class TelegramKeyboardReply(ABC):

    back = 'Назад [⬅️]'

    def __new__(self, resize_keyboard: bool = True, row_width: int = 1):
        self._meta = self.Meta
        return self.get_keyboard(self, resize_keyboard, row_width)

    def __add_back_button(self, buttons_objects: list):
        if not self._meta.main_menu:
            back_button = KeyboardButton(self.back)
            buttons_objects.append(back_button)

    def __get_keyboard_buttons(self) -> list[KeyboardButton]:
        keys = vars(self)
        buttons_objects = []

        for key in keys:
            if key.startswith('btn_'):
                button = keys.get(key)
                button = KeyboardButton(text=button)
                buttons_objects.append(button)

        self.__add_back_button(self, buttons_objects)
        return buttons_objects

    def get_keyboard(self, resize_keyboard: bool = True, row_width: int = 1):

        keyboard = ReplyKeyboardMarkup(
            row_width=row_width,
            resize_keyboard=resize_keyboard,
        )
        buttons = self.__get_keyboard_buttons(self)
        keyboard.add(*buttons)

        return keyboard

    class Meta:
        main_menu = False


class TelegramModelKeyboardReply(ABC):

    back = 'Назад [⬅️]'

    def __new__(self, queryset):
        self._meta = self.Meta
        assert self._meta.model is not None, 'Model must be in metaclass'
        assert self._meta.field is not None, 'Set field for model'

        if len(queryset) == 0:
            queryset = []

        return self.get_keyboard(self, queryset)

    def get_keyboard(self, queryset):
        keyboard = ReplyKeyboardMarkup(resize_keyboard=True)

        for data in queryset:
            field = getattr(data, self._meta.field)
            btn = KeyboardButton(field)
            keyboard.add(btn)
        
        back_btn = KeyboardButton(self.back)
        keyboard.add(back_btn)
        return keyboard

    class Meta:
        model = None
        field = None


class TelegramKeyboardInline(ABC):
    
    def __new__(self):
        return self.get_keyboard(self)

    def __valid_inline_button(self, button: dict) -> bool:
        if not isinstance(button, dict):
            return False

        text = button.get('text')
        callback = button.get('callback')

        if not text or not callback:
            return False
        return True

    def __get_buttons_data(self) -> list[dict]:
        keys = vars(self)
        buttons = []
        for key in keys:
            if key.startswith('btn_'):
                button = keys.get(key)
                assert self.__valid_inline_button(self, button), \
                    'Button format must be: {"text": "Text", "callback": "clb"}'
                buttons.append(button)
        assert len(buttons) > 0, \
                f'No found buttons in {self.__class__.__name__}'
        return buttons

    def __get_keyboard_buttons(self) -> list[InlineKeyboardButton]:
        buttons_objects = []
        buttons = self.__get_buttons_data(self)

        for button in buttons:
            text = button.get('text')
            callback = button.get('callback')
            button = InlineKeyboardButton(
                text=text, callback_data=callback,
            )
            buttons_objects.append(button)

        return buttons_objects

    def get_keyboard(self):
        buttons = self.__get_keyboard_buttons(self)
        keyboard = InlineKeyboardMarkup()
        keyboard.add(*buttons)
        return keyboard

    @classmethod
    def inline_keyboard_user(cls, user_callback_data: str) -> bool:
        buttons = cls.__get_buttons_data(cls)

        for button in buttons:
            callback = button.get('callback')
            if callback == user_callback_data:
                return True
        return False


class TelegramModelKeyboardInline(ABC):
    
    queryset = []

    def __new__(self, queryset: list, row_width=1):
        self._meta = self.Meta

        assert self._meta.model is not None, 'Model must be in metaclass'
        assert self._meta.fields is not None, 'Set field for model'

        if len(queryset) == 0:
            queryset = []

        self.queryset = queryset
        self.row_width = row_width
        self.child_name = self.__name__.lower()
        return self.get_keyboard(self)

    def __get_field_text(self, num: int, key) -> str:
        num += 1
        field_text = f'{num})'
        for field in self._meta.fields:
            text = getattr(key, field)
            if isinstance(text, bool):
                if text == True:
                    text = '✅'
                else:
                    text = '❌'
            else:
                text = str(text)
            field_text += '  ' + text
        return field_text

    def __get_inline_button(self, text: str, num: int) -> dict:
        if not isinstance(text, str):
            field = str(text)

        button_data = {
            'text': text,
            'callback': f'{self.child_name}_{num}'
        }
        return button_data

    def __get_buttons_data(self) -> list[dict]:
        buttons = []
        for num, key in enumerate(self.queryset):
            text = self.__get_field_text(self, num, key)
            button = self.__get_inline_button(self, text, num)
            buttons.append(button)
        return buttons

    def __get_keyboard_buttons(self) -> list[InlineKeyboardButton]:
        buttons_objects = []
        buttons = self.__get_buttons_data(self)

        for button in buttons:
            text = button.get('text')
            callback = button.get('callback')
            button = InlineKeyboardButton(
                text=text, callback_data=callback,
            )
            buttons_objects.append(button)

        return buttons_objects

    def get_keyboard(self):
        buttons = self.__get_keyboard_buttons(self)
        keyboard = InlineKeyboardMarkup(row_width=self.row_width)
        keyboard.add(*buttons)
        return keyboard

    @classmethod
    def inline_keyboard_user(cls, user_callback_data: str) -> bool:
        buttons = cls.__get_buttons_data(cls)

        for button in buttons:
            callback = button.get('callback')
            if callback in user_callback_data:
                return True
        return False

    @classmethod
    def get_text_button(self, request: CallbackQuery):
        data = request.data
        buttons = request.message.reply_markup.to_dict()
        buttons = buttons.get('inline_keyboard')

        for button in buttons:
            button = button[0]
            callback_data = button.get('callback_data')
            if callback_data == data:
                return button.get('text')
        return ''

    class Meta:
        model = None
        fields: list = None
